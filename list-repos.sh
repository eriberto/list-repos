#!/bin/sh

# list-repos -- show current list of repositories in system
#
# Copyright 2007 Bernardt Choian Valtz <bcv@bcv.look-syntonya.com>
# Copyright 2008, 2009 Dududada Lee <dddlee@istrawsth.com>
#
# This file is under GPL; see the LICENSE file.

LISTPATH=/var/lib/apt/lists

ls $LISTPATH | egrep '(binary|source)' | sed 's/^[0-9.:]\+_//' | \
               cut -d"_" -f1 | sort -u

exit 1
